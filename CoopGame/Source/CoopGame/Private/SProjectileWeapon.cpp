// Fill out your copyright notice in the Description page of Project Settings.

#include "SProjectileWeapon.h"
#include "Engine/World.h"




void ASProjectileWeapon::Fire()
{
	AActor* MyOwner = GetOwner(); // Actor that owns this gun
	if (MyOwner && ProjectileClass) {
		FVector EyeLocation;
		FRotator EyeRotation;
		MyOwner->GetActorEyesViewPoint(EyeLocation, EyeRotation);

		FVector MuzzleLocation = MeshComp->GetSocketLocation(MuzzleSocketName);

		FActorSpawnParameters SpawnParams;
		SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
		GetWorld()->SpawnActor<AActor>(ProjectileClass, FTransform(EyeRotation, MuzzleLocation), SpawnParams);

		//UE_LOG(LogTemp, Error, TEXT("Spawned : %s"), *Spawned->GetName());
		// Because line trace is not called, some particle effects aren't called along with it

	}
}